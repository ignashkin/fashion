// Main javascript entry point
// Should handle bootstrapping/starting application

'use strict';

import svg4everybody from "./svg4everybody";
import swiper from "./swiper"
svg4everybody();

// slider init

document.addEventListener("DOMContentLoaded", swiperInit);

function swiperInit() {
  const swiperArr = document.querySelectorAll(".swiper-container");
  const width = window.innerWidth;

  let mainSlider = new Swiper (swiperArr[0], {
    nextButton: '.main-slider__right',
    prevButton: '.main-slider__left',
    slidesPerView: 1,
  })
  let bannerMiddleSlider = new Swiper (swiperArr[1], {
    spaceBetween: 30,
    nextButton: '.middle-right',
    prevButton: '.middle-left',
    slidesPerView: width < 768 ? 1 : width < 1024 ? 2 : 2,
  })
  let bannerBottomSlider = new Swiper (swiperArr[2], {
    nextButton: '.bottom-right',
    prevButton: '.bottom-left',
    slidesPerView: width < 768 ? 1 : width < 1024 ? 2 : 2,
    spaceBetween: 30
  })
  let comboSlider = new Swiper (swiperArr[3], {
    nextButton: '.combo-right',
    prevButton: '.combo-left',
    slidesPerView: width<= 420 ? 1 : width <= 768 ? 2 : width <= 1024 ? 2 : 4,
    spaceBetween: 30
  })

};

// header hamburger button toggle
const icon = document.querySelector(".page-header__icon");
icon.addEventListener("click", iconToggle);

function iconToggle(e) {
  const nav = document.querySelector(".main-nav");
  icon.classList.toggle("open");
  nav.classList.toggle("main-nav--opened");
}
