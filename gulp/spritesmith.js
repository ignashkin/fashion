'use strict';

import path from 'path';
import spritesmith from 'gulp.spritesmith';

export default function(gulp, plugins, args, config, taskTarget, browserSync) {
  let dirs = config.directories;
  let dest = path.join(taskTarget, dirs.images.replace(/^_/, ''));
  gulp.task('sprite', () => {
  let spriteData = gulp.src(path.join(dirs.source, dirs.sprites,'**/*.png'))
  .pipe(spritesmith({
    imgName: 'sprite.png',
    cssName: 'sprite.less',
    padding: 20
  }));
  return spriteData.pipe(gulp.dest(dest));
});
}
