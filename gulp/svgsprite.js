'use strict';

import path from 'path';
import svgSprite from 'gulp-svg-sprites';
import svgmin from 'gulp-svgmin';
import cheerio from 'gulp-cheerio';
import replace from 'gulp-replace';

export default function(gulp, plugins, args, config, taskTarget, browserSync) {
  let dirs = config.directories;
  let dest = path.join(taskTarget, dirs.images.replace(/^_/, ''));
  gulp.task('svgSpriteBuild', function () {
  	return gulp.src(path.join(dirs.source, dirs.images, 'icons/*.svg'))
  		// minify svg
  		.pipe(svgmin({
  			js2svg: {
  				pretty: true
  			}
  		}))
  		// remove all fill and style declarations in out shapes
  		// .pipe(cheerio({
  		// 	run: function ($) {
  		// 		$('[fill]').removeAttr('fill');
  		// 		$('[style]').removeAttr('style');
  		// 	},
  		// 	parserOptions: { xmlMode: true }
  		// }))
  		// // cheerio plugin create unnecessary string '>', so replace it.
  		// .pipe(replace('&gt;', '>'))
  		// build svg sprite
  		.pipe(svgSprite({
  				mode: "symbols",
  				preview: false,
  				selector: "icon-%f",
  				svg: {
  					symbols: 'sprite.svg'
  				}
  			}
  		))
  		.pipe(gulp.dest(path.join(dest, 'icons/')));
  });
}
